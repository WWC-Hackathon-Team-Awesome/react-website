
# Seat Peek 1.0.0 
##### a WWC IoT Hack-A-Thon project
Giving people a sense of place, in a public space.
## A Smarter Bus Project
A way for the public to gauge the crowd quality of a public vehicle.
##### App: http://seatpeek.firebaseapp.com
##### WebSite: https://kaitlynfujihara.github.io/iot-hackathon18/
##### Repo: https://gitlab.com/WWC-Hackathon-Team-Awesome/react-website

## Dev Install
```shell
cd {project DIR}
yarn
yarn start
```
